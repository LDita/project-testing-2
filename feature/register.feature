Feature: Register
  As an unregister user
  I Want to sign up to Amazon website
  So That I can login Amazon website

  Scenario: Email already registered
    Given a registered user with the email "namakuldita@gmail.com" with password "coklat15" exists
    And I am on the "/signup" page
    When I fill in "name" with "Dita Linmas"
    And I fill in "Email" with "namakuldita@gmail.com"
    And I fill in "Password" with "coklat15"
    And I fill in "Verify Password" with "coklat15"
    And I press "Sign up"
    Then the signup form should be shown again
    And I should see "Email has already been taken"
    And I should not be signed in

  Scenario: Password is not valid
    Given I am on the "/signup" page
    When I fill in "name" with "Dita Linmas"
    And I fill in "Email" with "namakuldita@gmail.com"
    And I fill in "Password" with "coklat15"
    And I fill in "Verify Password" with "coklat11"
    And I press "Sign up"
    Then the signup form should be shown again
    And I should see "Password doesn't match confirmation"
    And I should not be registered
    And I should not be signed in