Feature: Login
  As a Login user
  I Want to sign in to Amazon website
  So That I can login Amazon website

  Scenario: Invalid email input
    Given a login user with the email "namakuldita@gmail..com"
    And I am on the "/signin" page
    When I fill in "Email" with "namakuldita@gmail.com"
    And I press "Continue"
    Then the signin form should be shown again
    And I should see "There was a problem"
    And I should not be signed in
